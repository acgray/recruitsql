from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from forms_builder.forms import urls as forms_urls

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from interviews.views import InterviewSessionDetailView, InterviewBookingWizard, INTERVIEW_BOOKING_FORMS, InterviewAppointmentBookingDeleteView
from jobs.views import PositionDetailView, JobApplicationDetailView, JobApplicationListView, PositionJobApplicationListView, JobApplicationDeleteView
from users.views import UserProfileUpdateView, UserUpdateView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'recruitsql.views.home', name='home'),
    # url(r'^recruitsql/', include('recruitsql.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^forms/', include(forms_urls)),
    #url(r'^$', PositionListView.as_view(), name="index"),
    url(r'^position/(?P<pk>\d+)$', PositionDetailView.as_view(), name='position_detail'),
    url(r'^apply/(?P<pk>\d+)$', 'jobs.views.application_form_view', name="job_apply"),

    url(r'^$', 'jobs.views.position_filter', name="index"),

    url(r'^accounts/profile$', login_required(UserProfileUpdateView.as_view()), name='update_profile'),
    (r'^accounts/', include('registration_withemail.urls')),
    (r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/check_profile$', 'users.views.check_profile_view', name="check_profile"),
    url(r'^accounts/cv/(?P<uid>\d+)$', 'users.views.get_cv_file', name='get_cv'),
    url(r'^accounts/applications$', login_required(JobApplicationListView.as_view()), name="jobapplication_list"),
    url(r'^accounts/user$', login_required(UserUpdateView.as_view()), name="user_update"),

    url(r'^jobapplication/(?P<pk>\d+)/$', login_required(JobApplicationDetailView.as_view()), name="jobapplication_detail"),
    url(r'position/(\d+)/jobapplications$', login_required(PositionJobApplicationListView.as_view()), name="position_jobapplication_list"),
    url(r'^jobapplication/(?P<pk>\d+)/edit$', 'jobs.views.application_form_view', name='jobapplication_edit', kwargs={'mode': 'edit'}),
    url(r'^jobapplication/(?P<pk>\d+)/delete', login_required(JobApplicationDeleteView.as_view()), name='jobapplication_delete'),

    url(r'^interviews/session/(?P<pk>\d+)/$', InterviewSessionDetailView.as_view(), name="interviewsession_detail"),
    url(r'^interviews/booking$', InterviewBookingWizard.as_view(INTERVIEW_BOOKING_FORMS), name="interview_booking_wizard"),
    url(r'^interviews/booking_start/(?P<request_id>\d+)$', 'interviews.views.booking_start', name="interview_booking_start"),
    url(r'^interviews/booking/(?P<pk>\d+)/cancel$', InterviewAppointmentBookingDeleteView.as_view(), name="interview_appointment_cancel"),
    url(r'^accounts/interviews$', 'interviews.views.user_interviews_view', name="interview_request_list"),

    (r'^manage/', include('jobs_admin.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

