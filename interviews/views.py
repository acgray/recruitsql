# Create your views here.
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.generic import DetailView, DeleteView
from interviews.forms import AppointmentSelectForm, InterviewAppointmentBookingForm, InterviewSessionSelectForm
from interviews.models import InterviewAppointmentBooking, InterviewSession, InterviewRequest

INTERVIEW_BOOKING_FORMS = [
    ('session_select', InterviewSessionSelectForm),
    ('appointment_select', AppointmentSelectForm),
    ('appointment_booking', InterviewAppointmentBookingForm),
]

def booking_start(request, request_id):
    ir = get_object_or_404(InterviewRequest, id=request_id)
    request.session['interview_request'] = ir
    return redirect('interview_booking_wizard')

class InterviewBookingWizard(SessionWizardView):

    def get_template_names(self):
        return ["interviews/booking_wizard/%s.html" % self.steps.current, 'interviews/booking_wizard.html']

    def done(self, form_list, **kwargs):
        data = self.get_all_cleaned_data()
        ir = self.storage.data.pop('interview_request')
        booking = InterviewAppointmentBooking(
            appointment = data['appointment'],
            booked_by = self.request.user,
            application = ir.job_application,
        )
        booking.save()
        ir.booking = booking
        ir.save()
        context = RequestContext(self.request)
        context['booking'] = booking

        return render_to_response('interviews/booking_wizard_done.html', context)

    def get_form_kwargs(self, step=None):
        kwargs = {}
        if step == 'session_select':
            ir = self.request.session.get('interview_request',None)
            if ir:
                self.storage.data['interview_request'] = ir
                kwargs.update({'allowed_sessions': ir.interview_sessions.all()})
        elif step == 'appointment_select':
            session = self.get_cleaned_data_for_step('session_select')['session']
            kwargs.update({'session': session})
        return kwargs

    def get(self, request, *args, **kwargs):
        if not self.request.session.get('interview_request'):
            return redirect('interview_request_list')
        if not self.request.session['interview_request'].is_valid():
            context = RequestContext(request)
            return render_to_response('500.html', context)
        return super(InterviewBookingWizard,self).get(request, *args, **kwargs)



    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(InterviewBookingWizard, self).dispatch(request, *args, **kwargs)

class InterviewSessionDetailView(DetailView):
    model = InterviewSession

def user_interviews_view(request):
    context = RequestContext(request)

    context.update({
        'interview_requests': InterviewRequest.objects.filter(job_application__in=request.user.jobapplication_set.all()),
        'interview_bookings': InterviewAppointmentBooking.objects.filter(application__in=request.user.jobapplication_set.all()) #, appointment__start_time__gt=timezone.now())
    })

    return render_to_response('interviews/interviewrequest_list.html', context)

class InterviewAppointmentBookingDeleteView(DeleteView):
    model = InterviewAppointmentBooking

    def get_queryset(self):
        return InterviewAppointmentBooking.objects.filter(application__in=self.request.user.jobapplication_set.all())

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, "Your appointment has been cancelled. You may now be able to rebook using the link below.")
        return reverse_lazy('interview_request_list')


