from django import forms
from interviews.models import InterviewAppointmentBooking, InterviewAppointment, InterviewSession


class InterviewAppointmentBookingForm(forms.ModelForm):
    class Meta:
        model = InterviewAppointmentBooking
        exclude = ('booked_at', 'appointment', 'application', 'booked_by',)

    def save(self, commit=True):
        instance = super(InterviewAppointmentBookingForm,self).save(commit=False)
        if commit:
            instance.save()
        return instance


class InterviewSessionSelectForm(forms.Form):
    def __init__(self, **kwargs):
        allowed_sessions = kwargs.pop('allowed_sessions', None)
        super(InterviewSessionSelectForm,self).__init__(**kwargs)
        if allowed_sessions:
            self.fields['session'].queryset = allowed_sessions

    session = forms.ModelChoiceField(queryset=InterviewSession.objects.all())

class AppointmentSelectForm(forms.Form):
    def __init__(self, **kwargs):
        session = kwargs.pop('session', None)
        super(AppointmentSelectForm,self).__init__(**kwargs)
        if session:
            self.fields['appointment'].queryset = InterviewAppointment.objects.available(session=session)

    appointment = forms.ModelChoiceField(queryset=InterviewAppointment.objects.available())