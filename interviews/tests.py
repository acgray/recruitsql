"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.utils import timezone
from interviews.models import InterviewRequest, InterviewAppointmentBooking, InterviewSession


class InterviewRequestTestCase(TestCase):

    def test_is_expired(self):
        """ Request should be expired if it has a deadline which is in the past """

        now = timezone.now()
        request = InterviewRequest()

        # No deadline never expires
        self.assertFalse(request.is_expired())

        # Future deadline should not expire
        request.deadline = now + timezone.timedelta(days=10)
        self.assertFalse(request.is_expired())

        # Past deadline should be expired
        request.deadline = now - timezone.timedelta(days=1)
        self.assertTrue(request.is_expired())

    def test_is_valid(self):
        """ Valid if there is no associated booking and is not expired """

        # Only valid if it doesn't have a booking and isn't expired
        request = InterviewRequest()
        self.assertTrue(request.is_valid())

        # With a booking = invalid
        request2 = InterviewRequest(booking=InterviewAppointmentBooking())
        self.assertFalse(request2.is_valid())

        # Expired = invalid
        request3 = InterviewRequest(deadline=timezone.now()-timezone.timedelta(days=1))
        self.assertFalse(request3.is_valid())

        # Both = invalid
        request3.booking = InterviewAppointmentBooking()
        self.assertFalse(request3.is_valid())

        self.assertIsInstance(request.get_status(), basestring)  # Str or unicode
        self.assertIsInstance(request2.get_status(), basestring)
        self.assertIsInstance(request3.get_status(), basestring)

class InterviewSessionTestCase(TestCase):

    def setUp(self):
        now = timezone.now()
        self.session = InterviewSession.objects.create(start_date=now, end_date=now+timezone.timedelta(hours=1))

    def test_generate_appointments(self):

        # 60 min / 10 min = should generate 6 appointments
        self.session.generate_appointments(10)
        self.assertEqual(self.session.interviewappointment_set.count(), 6)

    def test_generate_appointments_with_break(self):

        # 60 min / (10 + 2) = 5 appointments
        self.session.generate_appointments(10, 2)
        self.assertEqual(self.session.interviewappointment_set.count(), 5)

