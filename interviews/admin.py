from django import forms
from django.contrib import admin, messages
from django.shortcuts import render_to_response
from django.template import RequestContext
from interviews.models import InterviewSession, InterviewAppointment, InterviewRequest, InterviewAppointmentBooking


class InterviewAppointmentInline(admin.TabularInline):
    model = InterviewAppointment
    # readonly_fields = ('start_time','end_time',)
    extra = 0

class InterviewSessionAdmin(admin.ModelAdmin):
    inlines = [InterviewAppointmentInline,]
    list_display = ('start_date', 'end_date','location',)
    actions = ['generate_appointments',]

    class GenerateAppointmentsForm(forms.Form):
        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
        action = forms.CharField(widget=forms.HiddenInput)

        appointment_length = forms.IntegerField()
        break_length = forms.IntegerField()

    def generate_appointments(self, request, queryset):
        form = None
        context = RequestContext(request)

        if 'submit' in request.POST:
            form = self.GenerateAppointmentsForm(request.POST or None)
            if form.is_valid():
                for session in queryset:
                    session.generate_appointments(
                        appt_length=form.cleaned_data['appointment_length'],
                        break_length=form.cleaned_data['break_length']
                    )
                messages.add_message(request, messages.SUCCESS, "Appointments created for %s interview sessions" % queryset.count())
                return

        else:
            form = self.GenerateAppointmentsForm(initial={'action': request.POST.get('action'),'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

        context['queryset'] = queryset
        context['form'] = form
        return render_to_response('admin/interviews/interviewsession_generate_appts.html', context)


admin.site.register(InterviewSession, InterviewSessionAdmin)
admin.site.register(InterviewRequest)
admin.site.register(InterviewAppointmentBooking)