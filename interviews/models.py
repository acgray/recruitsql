from datetime import timedelta
from django.conf import settings
from django.db import models

# Create your models here.
import math
from django.utils import timezone
from django.utils.formats import date_format

class InterviewRequest(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    job_application = models.ForeignKey('jobs.JobApplication')
    interview_sessions = models.ManyToManyField('InterviewSession')
    booking = models.ForeignKey('InterviewAppointmentBooking', blank=True, null=True, on_delete=models.SET_NULL)
    deadline = models.DateTimeField(blank=True, null=True)

    def is_expired(self):
        if self.deadline:
            return self.deadline < timezone.now()
        return False

    def is_valid(self):
        return not bool(self.booking) and not self.is_expired()

    def get_status(self):
        if self.booking:
            return 'Complete'
        elif self.deadline and self.deadline < timezone.now():
            return 'Expired'
        else:
            return 'Open'



class InterviewSession(models.Model):
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)

    location = models.CharField(blank=True, max_length=255)
    extra_instructions = models.TextField(blank=True, null=True)

    def __unicode__(self):
        if self.start_date.date() == self.end_date.date():
            date_str = "%s, %s - %s" % (
                date_format(self.start_date.date(), 'DATE_FORMAT', use_l10n=True),
                date_format(self.start_date.time(), 'TIME_FORMAT', use_l10n=True),
                date_format(self.end_date.time(), 'TIME_FORMAT', use_l10n=True),
            )
        else:
            date_str = "%s - %s" % (self.start_date, self.end_date,)

        if self.location:
            return "%s (%s)" % (date_str, self.location)
        else:
            return date_str

    def generate_appointments(self, appt_length, break_length=0):
        session_delta = self.end_date - self.start_date
        avail_delta = session_delta + timedelta(minutes=break_length)
        each_seconds = appt_length*60 + break_length*60
        each_delta = timedelta(seconds=each_seconds)
        num_appts = math.floor(avail_delta.total_seconds()/each_seconds)
        next_start = self.start_date
        for i in range(0, int(num_appts), 1):
            InterviewAppointment.objects.create(
                session=self,
                start_time=next_start,
                end_time=next_start+timedelta(seconds=appt_length*60),
            )
            next_start += each_delta

class InterviewAppointmentBookingManager(models.Manager):
    def available(self, **kwargs):
        return self.filter(interviewappointmentbooking=None, **kwargs)


class InterviewAppointment(models.Model):
    session = models.ForeignKey('InterviewSession')
    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)

    objects = InterviewAppointmentBookingManager()

    def __unicode__(self):
        return date_format(self.start_time, 'DATETIME_FORMAT', use_l10n=True)

    def is_booked(self):
        return self.interviewappointmentbooking_set.count() > 0

class InterviewAppointmentBooking(models.Model):
    appointment = models.ForeignKey('InterviewAppointment')
    application = models.ForeignKey('jobs.JobApplication')
    booked_at = models.DateTimeField(auto_now_add=True)
    booked_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    notes = models.TextField(blank=True)

