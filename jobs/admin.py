from django import forms
from django.conf.urls import patterns, url
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.db.models import FieldDoesNotExist
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, Template, Context
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from jobs.forms import PositionModelForm
from jobs.models import Organisation, Position, Period, JobApplication, ApplicationForm, Field, FieldEntry, Note, JobApplicationGroup, FileAttachment
from notifications.helpers import send_template_to_queryset
from notifications.models import EmailNotification, EmailTemplate

admin.site.register(Organisation)
admin.site.register(Period)


class CSVAdmin(admin.ModelAdmin):
    """
    Adds a CSV export action to an admin view.
    """

    # This is the maximum number of records that will be written.
    # Exporting massive numbers of records should be done asynchronously.
    csv_record_limit = 1000

    extra_csv_fields = ()

    def get_actions(self, request):
        actions = self.actions if hasattr(self, 'actions') else []
        actions.append('csv_export')
        actions = super(CSVAdmin, self).get_actions(request)
        return actions

    def get_extra_csv_fields(self, request):
        return self.extra_csv_fields

    def csv_export(self, request, qs=None, *args, **kwargs):
        import csv
        from django.http import HttpResponse
        from django.template.defaultfilters import slugify

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' \
                                          % slugify(self.model.__name__)
        headers = list(self.list_display) + list(self.get_extra_csv_fields(request))
        writer = csv.DictWriter(response, headers)

        # Write header.
        header_data = {}
        for name in headers:
            if hasattr(self, name) \
                and hasattr(getattr(self, name), 'short_description'):
                header_data[name] = getattr(
                    getattr(self, name), 'short_description')
            else:
                try:
                    field = self.model._meta.get_field_by_name(name)
                    if field and field[0].verbose_name:
                        header_data[name] = field[0].verbose_name
                    else:
                        header_data[name] = name
                except FieldDoesNotExist:
                    header_data[name] = name
            header_data[name] = header_data[name].title()
        writer.writerow(header_data)

        # Write records.
        for r in qs[:self.csv_record_limit]:
            data = {}
            for name in headers:
                if hasattr(r, name):
                    data[name] = getattr(r, name)
                elif hasattr(self, name):
                    data[name] = getattr(self, name)(r)
                elif '__' in name:
                    lookup = name.split('__')
                    data[name] = r
                    for x in lookup:
                        if hasattr(data[name], x):
                            data[name] = getattr(data[name], x)
                else:
                    raise Exception, 'Unknown field: %s' % (name,)

                if callable(data[name]):
                    data[name] = data[name]()
            data = {k: unicode(v).encode('utf8') for k, v in data.items()}
            writer.writerow(data)
        return response

    csv_export.short_description = \
        'Exported selected %(verbose_name_plural)s as CSV'


class FileAttachmentInline(admin.StackedInline):
    model = FileAttachment
    extra = 1


class PositionModelAdmin(CSVAdmin):
    form = PositionModelForm
    list_display = ('title', 'organisation', 'period', 'form', 'public', 'admin_links')
    list_filter = ('organisation', 'period')
    actions = ['make_public']
    inlines = [FileAttachmentInline, ]

    def jobapplications_view(self, request, position_id):
        """
        List job applications for this position
        """
        context = RequestContext(request)
        position = get_object_or_404(Position, id=position_id)
        context['object_list'] = position.jobapplication_set.all()
        context['position'] = position
        return render_to_response('admin/jobs/position_jobapplications.html', context)

    def jobapplication_detail_view(self, request, position_id, jobapplication_id):
        """
        View job application detail
        """
        context = RequestContext(request)
        context['position'] = get_object_or_404(Position, id=position_id)
        context['object'] = get_object_or_404(JobApplication, id=jobapplication_id, position_id=position_id)

        return render_to_response('admin/jobs/position_jobapplication_detail.html', context)

    def make_public(self, request, queryset):
        queryset.update(public=True)
        messages.add_message(request, messages.SUCCESS, "%s positions are now public" % queryset.count())

    def get_urls(self):
        urls = super(PositionModelAdmin, self).get_urls()
        extra_urls = patterns('',
                              url('^(?P<position_id>\d+)/applications/$',
                                  self.admin_site.admin_view(self.jobapplications_view),
                                  name="position_jobapplications"),
                              url('^(?P<position_id>\d+)/applications/(?P<jobapplication_id>\d+)',
                                  self.admin_site.admin_view(self.jobapplication_detail_view),
                                  name="position_jobapplication_detail"),
        )
        return extra_urls + urls


admin.site.register(Position, PositionModelAdmin)


class FieldAdmin(admin.TabularInline):
    model = Field
    exclude = ('slug',)


class ApplicationFormAdmin(admin.ModelAdmin):
    inlines = (FieldAdmin,)


admin.site.register(ApplicationForm, ApplicationFormAdmin)


class FieldEntryInline(admin.TabularInline):
    model = FieldEntry
    exclude = ('field',)
    fields = ('render_value_html',)
    readonly_fields = ('render_value_html',)
    can_delete = False
    extra = 0

    def render_value_html(self, obj):
        return mark_safe(obj.render_value_html())

    render_value_html.short_description = 'Entry values'

    def has_add_permission(self, request):
        return False


class EmailNotificationInline(admin.StackedInline):
    model = EmailNotification
    can_delete = True
    readonly_fields = ('from_address', 'sent_date', 'subject', 'body', 'template')
    extra = 0

    def has_add_permission(self, request):
        return False


class NoteInline(admin.StackedInline):
    model = Note
    readonly_fields = ['author', 'created']
    extra = 0

    def save_model(self, request, obj, form, change):
        if not obj.author:
            obj.author = request.user
        obj.save()


class JobApplicationAdmin(CSVAdmin):
    inlines = (FieldEntryInline, EmailNotificationInline, NoteInline)
    list_display = ("application_code", "user", "position", "organisation", "groups_display", "status", "entry_time")
    extra_csv_fields = ("user__profile__first_name", "user__profile__last_name")
    list_filter = ('status', 'position', 'position__organisation', 'groups',)
    readonly_fields = ('user_details_html', 'form')
    actions = ['send_email_to_selected', 'reject_selected']
    filter_horizontal = ['groups', ]

    def queryset(self, request):
        return self.model.objects.filter(position__organisation__in=request.site.organisation_set.all())

    def save_formset(self, request, form, formset, change):
        """
        Save notes with the current user as author
        """
        if formset.model == Note:
            def set_user(instance):
                if not instance.author_id:
                    instance.author = request.user
                    instance.save()

            instances = formset.save(commit=False)
            map(set_user, instances)
            formset.save_m2m()
            return instances
        else:
            return super(JobApplicationAdmin, self).save_formset(request, form, formset, change)

    class RejectionConfirmationForm(forms.Form):
        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
        action = forms.CharField(widget=forms.HiddenInput)
        send_email = forms.BooleanField()
        email_template = forms.ModelChoiceField(queryset=EmailTemplate.objects.all(), required=False)

        def clean_email_template(self):
            print self.cleaned_data
            if self.cleaned_data['send_email'] and not self.cleaned_data['email_template']:
                raise ValidationError("You must choose an email template")
            return self.cleaned_data['email_template']

    def reject_selected(self, request, queryset):
        """ Rejects selected candidates and sends associated email notification """
        form = None
        if 'submit' in request.POST:
            form = self.RejectionConfirmationForm(request.POST)
            if form.is_valid():
                if form.cleaned_data['send_email']:
                    send_template_to_queryset(form.cleaned_data['email_template'], queryset)
                queryset.update(status='R')
                messages.add_message(request, messages.SUCCESS,
                                     '%s applications successfully rejected' % queryset.count())
                return

        if not form:
            form = self.RejectionConfirmationForm(initial={'action': request.POST.get('action'),
                                                           '_selected_action': request.POST.getlist(
                                                               admin.ACTION_CHECKBOX_NAME)})
        context = RequestContext(request)
        context['queryset'] = queryset
        context['form'] = form
        return render_to_response('admin/jobs/jobapplication_reject.html', context)

    class SendEmailForm(forms.Form):
        """
        Form to send a custom email notification to a single candidate or queryset
        """
        _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
        template = forms.ModelChoiceField(EmailTemplate.objects, required=False)
        action = forms.CharField(widget=forms.HiddenInput)

        from_address = forms.EmailField(required=False)
        subject = forms.CharField()
        body = forms.CharField(widget=forms.Textarea, help_text='Available context: {{ object }} for the application')

    def invite_selected_to_interview(self, request, queryset):
        """
        Invite a group of candidates to interview (not implemented)
        """
        pass

    def send_email_to_selected(self, request, queryset):
        """
        Send custom email to a candidate or group
        """
        form = None
        context = RequestContext(request)
        context['applications'] = queryset

        if 'preview' in request.POST or 'submit' in request.POST:
            form = self.SendEmailForm(request.POST)
            render_context = Context({'object': queryset[0]})
            if form.is_valid():
                rendered_data = {
                    'subject': Template(form.cleaned_data['subject']).render(render_context),
                    'body': Template(form.cleaned_data['body']).render(render_context),
                }
                if 'preview' in request.POST:
                    context['preview'] = rendered_data

                if 'submit' in request.POST:
                    for ja in queryset:
                        EmailNotification.objects.create(
                            job_application=ja,
                            template=form.cleaned_data['template'],
                            subject=rendered_data['subject'],
                            body=rendered_data['body'],
                            from_address=form.cleaned_data['from_address']
                        )

                    messages.add_message(request, messages.SUCCESS,
                                         "%s Email notifications successfully sent" % queryset.count())
                    return

        if not form:
            form = self.SendEmailForm(initial={'action': request.POST.get('action'),
                                               '_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

        context['form'] = form

        return render_to_response('admin/jobs/jobapplication_email.html', context)

    def user_details_html(self, obj):
        output = mark_safe(render_to_string('admin/_user_detail.html', {'user': obj.user}).strip())
        return output

    user_details_html.short_description = 'User details'
    user_details_html.allow_tags = True

    def groups_display(self, obj):
        return ', '.join([group.name for group in obj.groups.all()])

    groups_display.short_description = "Groups"


admin.site.register(JobApplication, JobApplicationAdmin)
admin.site.register(JobApplicationGroup)
