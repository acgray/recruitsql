from django import forms
from forms_builder.forms.forms import FormForForm
from pagedown.widgets import AdminPagedownWidget
from jobs.models import Position, FieldEntry, JobApplication

attrs_dict = {'class': 'required'}


class PositionModelForm(forms.ModelForm):
    job_description = forms.CharField(widget=AdminPagedownWidget())

    class Meta:
        model = Position


class FormForApplicationForm(FormForForm):
    field_entry_model = FieldEntry

    def clean(self):
        if self.data['action'] and self.data['action'] == 'submit':
            for key, value in self.cleaned_data.iteritems():
                field = self.form.fields.get(slug=key)
                if field.required_for_submission and not value:
                    self._errors[key] = self.error_class(["This field is required before submission"])

        return self.cleaned_data

    # def save(self, **kwargs):
    #     # entry = super(FormForApplicationForm,self).save(**kwargs)
    #     # if self.data['action'] and self.data['action'] == 'submit':
    #     #     entry.status = 'S'
    #     #
    #     # return entry
    #     return super(FormForApplicationForm,self).save(**kwargs)

    class Meta(FormForForm.Meta):
        model = JobApplication
        exclude = ('form', 'user', 'entry_time', 'position', 'status', 'groups')