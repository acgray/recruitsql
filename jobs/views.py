# Create your views here.
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import get_current_site
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404, redirect, resolve_url
from django.template import RequestContext
from django.views.generic import ListView, DetailView, DeleteView
from jobs.filters import PositionFilterSet
from jobs.forms import FormForApplicationForm
from jobs.models import Position, JobApplication, ApplicationForm
from users.models import UserProfile


class PositionListView(ListView):
    model = Position

    def get_queryset(self):
        site = get_current_site(self.request)
        orgs = site.organisation_set.all()
        return self.model.objects.public(organisation__in=orgs).order_by('closing_date')

    def get_context_data(self, **kwargs):
        context = super(PositionListView, self).get_context_data(**kwargs)
        context['site'] = get_current_site(self.request)
        return context


def position_filter(request):
    f = PositionFilterSet(request.GET, queryset=Position.objects.public().filter(
        organisation__in=request.site.organisation_set.all()).order_by('closing_date'))
    context = RequestContext(request)
    context['filter'] = f
    return render_to_response('jobs/position_filter.html', context)


class PositionDetailView(DetailView):
    model = Position

    def get_queryset(self):
        return self.model.objects.public().filter(organisation__in=self.request.site.organisation_set.all())


class JobApplicationDetailView(DetailView):
    model = JobApplication

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class JobApplicationDeleteView(DeleteView):
    model = JobApplication

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, "The application has been successfully deleted.")
        return self.request.GET.get('next') or reverse_lazy('jobapplication_list')

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user, status='D')


@login_required
def application_form_view(request, *args, **kwargs):
    if kwargs.get('mode', '') == 'edit':
        jobapp = get_object_or_404(JobApplication.objects.filter(user=request.user, status='D'), id=kwargs.get('pk'))
        af = jobapp.form
        position = jobapp.position
    else:
        jobapp = None
        try:
            position = Position.objects.open_and_public().get(id=kwargs.get('pk'))
            af = position.form
        except (ApplicationForm.DoesNotExist, Position.DoesNotExist):
            raise Http404

    # Redirect if position has closed
    if position.has_closed():
        messages.add_message(request, messages.ERROR, "That position has now closed.")
        return redirect('jobapplication_list')

    request_context = RequestContext(request)
    form_for_form = FormForApplicationForm(af, request_context, request.POST or None, request.FILES or None,
                                           instance=jobapp)

    # Check user profile and redirect if they don't have one or it's not complete
    try:
        profile = request.user.profile
        assert profile.is_complete(position.require_cv)
    except (AssertionError, UserProfile.DoesNotExist):
        if position.require_cv:
            errmsg = "You must complete your profile and upload a CV before submitting application for this position"
        else:
            errmsg = "You must complete your profile before submitting an application for this position"

        messages.add_message(request, messages.ERROR,
                             "You must complete your profile and upload a CV before submitting an application.")
        return HttpResponseRedirect(
            str(reverse_lazy('update_profile')) + '?next=' + str(resolve_url('job_apply', af.id)))

    if not kwargs.get('mode', '') == 'edit':
        try:
            existing_app = JobApplication.objects.get(user=request.user, form=af)
            return render_to_response('existing_application.html', {'ja': existing_app}, request_context)
        except JobApplication.DoesNotExist:
            pass

    if request.method == "POST":
        if form_for_form.is_valid():
            ja = form_for_form.save(commit=False)
            ja.user = request.user
            if not ja.position:
                ja.position = position
            ja.save()

            if request.POST.get('action') == 'save':
                messages.add_message(request, messages.SUCCESS, "Your application has been successfully saved.")
                return redirect('jobapplication_list')
                #return render_to_response('jobs/jobapplication_save_success.html', {'jobapplication': ja}, request_context)
            else:
                ja.submit()
                return render_to_response('jobs/jobapplication_submit_success.html', {'jobapplication': ja},
                                          request_context)

    return render_to_response('test.html', {'form': form_for_form}, request_context)

# @login_required
# def application_form_edit(request, *args, **kwargs):
#     # Check user profile and redirect if they don't have one or it's not complete
#     try:
#         profile = request.user.profile
#         assert profile.is_complete()
#     except (AssertionError,UserProfile.DoesNotExist):
#         messages.add_message(request, messages.ERROR, "You must complete your profile before submitting an application.")
#         return redirect('update_profile')
#
#     jobapp = get_object_or_404(JobApplication.objects.filter(user=request.user, status='D'), id=kwargs.get('pk'))
#     request_context = RequestContext(request)
#
#     form_for_form = FormForApplicationForm(jobapp.form, request_context, request.POST or None, request.FILES or None, instance=jobapp)
#
#     if request.method == "POST":
#
#
#         if form_for_form.is_valid():
#             jobapp = form_for_form.save()
#
#         if request.POST.get('action') == 'save':
#             #return render_to_response('jobs/jobapplication_save_success.html', {'jobapplication': jobapp}, request_context)
#             messages.add_message(request, messages.SUCCESS, "Your application has been successfully saved.")
#             return redirect('jobapplication_list')
#         else:
#             jobapp.status = 'S'
#             jobapp.save()
#             return render_to_response('jobs/jobapplication_submit_success.html', {'jobapplication': jobapp}, request_context)
#
#
#     return render_to_response('test.html', {'form': form_for_form}, request_context)



class JobApplicationListView(ListView):
    model = JobApplication

    def get_queryset(self):
        user = self.request.user

        return JobApplication.objects.filter(user=user,
                                             position__organisation__in=self.request.site.organisation_set.all())

    def get_context_data(self, **kwargs):
        context = super(JobApplicationListView, self).get_context_data(**kwargs)

        context['draft'] = self.model.objects.draft(user=self.request.user)
        context['submitted'] = self.model.objects.submitted(user=self.request.user)

        return context


class PositionJobApplicationListView(ListView):
    model = JobApplication
    template_name = "jobs/position_jobapplication_list.html"

    def get_queryset(self):
        self.position = Position.objects.get(id=self.args[0])
        return self.position.form.jobapplication_set.all()

    def get_context_data(self, **kwargs):
        context = super(PositionJobApplicationListView, self).get_context_data(**kwargs)

        context['position'] = self.position

        return context
