from datetime import datetime
from os.path import basename
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import linebreaks
from forms_builder.forms.fields import FILE, TEXTAREA
from forms_builder.forms.models import AbstractFormEntry, AbstractField
import forms_builder
from django.utils.translation import ugettext_lazy as _


class Organisation(models.Model):
    """ Parent entity which owns a branded Site and posts jobs """

    name = models.CharField(max_length=255)
    logo = models.ImageField(upload_to='organisation_logos', blank=True)
    site = models.ForeignKey(to=Site)
    default_emailfrom = models.EmailField(blank=True, help_text="notification emails will be sent from this address",
                                          null=True)

    code = models.CharField(max_length=5, help_text="Used to identify organisation in job application ids", blank=True,
                            null=True)

    def __unicode__(self):
        return unicode(self.name)


class Period(models.Model):
    """ A period for time-limited positions.  E.g. Michaelmas Term 2014, 2014-2015, etc """

    name = models.CharField(max_length=255)

    def __unicode__(self):
        return unicode(self.name)


class PositionManager(models.Manager):
    def public(self, **kwargs):
        return self.filter(public=True, **kwargs)

    def open_and_public(self):
        return self.filter(public=True).exclude(closing_date__lt=datetime.now())

    def get_for_site(self, site):
        return self.filter(organisation__in=site.organisation)


class Position(models.Model):
    """ A job posting """

    title = models.CharField(max_length=255)
    organisation = models.ForeignKey(to=Organisation)
    period = models.ForeignKey(to=Period)
    job_description = models.TextField()
    summary = models.TextField(blank=True)

    public = models.BooleanField(default=True)
    closing_date = models.DateTimeField(blank=True, null=True)
    require_cv = models.BooleanField(default=True)

    form = models.ForeignKey('ApplicationForm', blank=True, null=True)
    applicants = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, through='JobApplication')

    objects = PositionManager()

    def __unicode__(self):
        return unicode(self.title)

    def has_closed(self):
        if self.closing_date:
            return self.closing_date < timezone.now()
        return False

    def is_open(self):
        return not self.has_closed()

    def get_absolute_url(self):
        return reverse('position_detail', args=[self.id])

    def has_form(self):
        return self.form is not None

    def admin_links(self):
        kw = {"args": (self.id,)}

        links = [
            ("View on site", self.get_absolute_url()),
            ("Show applicants", reverse('admin:position_jobapplications', **kw)),
        ]

        links_html = ['<a href="%s">%s</a>' % (url, text) for i, (text, url) in enumerate(links)]
        return "<br>".join(links_html)

    admin_links.allow_tags = True


class ApplicationForm(models.Model):
    """ Dynamically generated form which candidates use to apply for positions """

    name = models.CharField(max_length=255, blank=True, help_text="Internal use only", null=True)
    intro_text = models.TextField(blank=True)

    def list_fields(self):
        return self.fields.filter(show_in_list=True)

    def __unicode__(self):
        return unicode(self.name or 'Application Form %s' % self.id)


APPLICATION_STATUS_CHOICES = (
    ('D', 'Draft'),
    ('S', 'Submitted'),
    ('R', 'Rejected'),
)


class JobApplicationManager(models.Manager):
    def submitted(self, **kwargs):
        return self.filter(status='S', **kwargs)

    def draft(self, **kwargs):
        return self.filter(status='D', **kwargs)


class JobApplication(AbstractFormEntry):
    form = models.ForeignKey(ApplicationForm)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, parent_link=True)
    position = models.ForeignKey(Position, null=True)
    groups = models.ManyToManyField(to='JobApplicationGroup', blank=True)

    status = models.CharField(choices=APPLICATION_STATUS_CHOICES, max_length=1, default='D')

    objects = JobApplicationManager()

    def list_values(self):
        list_fields = self.form.list_fields()
        return self.fields.filter(field_id__in=[f.id for f in list_fields])

    class Meta:
        verbose_name = "Job Application"
        verbose_name_plural = "Job Applications"

    def __unicode__(self):
        return "Application #%s" % (self.application_code())

    @property
    def organisation(self):
        if self.position:
            return self.position.organisation

    def application_code(self):
        if self.position:
            return "%s-%02d-%03d" % (self.position.organisation.code, self.position_id, self.id)

    application_code.short_description = "ID"

    @property
    def is_draft(self):
        return self.status == 'D'

    def submit(self):
        self.status = 'S'
        self.email_notifications.create(
            from_address=self.position.organisation.default_emailfrom,
            subject='Application confirmation: #%s' % self.application_code(),
            body=render_to_string('jobs/jobapplication_submitted_email.txt',
                                  {'jobapplication': self, 'user': self.user})
        )
        self.save()


class FieldManager(forms_builder.forms.models.FieldManager):
    use_for_related_fields = True

    def in_list(self):
        return super(FieldManager, self).get_query_set().filter(show_in_list=True)


class Field(AbstractField):
    """
    Implements automated field ordering. (Borrowed from forms_builder.forms.models.Field
    """

    form = models.ForeignKey("ApplicationForm", related_name="fields")
    order = models.IntegerField(_("Order"), null=True, blank=True)
    show_in_list = models.BooleanField(default=False)
    required_for_submission = models.BooleanField(default=False)

    objects = FieldManager()

    class Meta(AbstractField.Meta):
        ordering = ("order",)

    def save(self, *args, **kwargs):
        if self.order is None:
            self.order = self.form.fields.count()
        super(Field, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        fields_after = self.form.fields.filter(order__gte=self.order)
        fields_after.update(order=models.F("order") - 1)
        super(Field, self).delete(*args, **kwargs)


class FieldEntry(models.Model):
    entry = models.ForeignKey(JobApplication, related_name="fields")

    field = models.ForeignKey(to=Field)
    value = models.CharField(max_length=forms_builder.forms.settings.FIELD_MAX_LENGTH,
                             null=True, blank=True)

    # def __init__(self, *args, **kwargs):
    #     super(FieldEntry,self).__init__(*args, **kwargs)
    #     value = models.CharField(max_length=forms_builder.forms.settings.FIELD_MAX_LENGTH,null=True,blank=True)
    #     value.contribute_to_class(self, 'value')

    class Meta:
        verbose_name = _("Form field entry")
        verbose_name_plural = _("Form field entries")

    def __unicode__(self):
        return self.get_field().label

    def get_field(self):
        return self.field

    def render_value_html(self):

        if self.field.is_a(FILE):
            return "<a href='%s'>Uploaded file</a>" % self.value

        if self.field.is_a(TEXTAREA):
            return linebreaks(self.value)

        return self.value


class Note(models.Model):
    job_application = models.ForeignKey(to='JobApplication')
    author = models.ForeignKey(to=settings.AUTH_USER_MODEL)
    content = models.TextField()
    created = models.DateTimeField(auto_created=True)

    def save(self, **kwargs):
        if not self.created:
            self.created = datetime.now()
        return super(Note, self).save(**kwargs)


class JobApplicationGroup(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return unicode(self.name)


class FileAttachment(models.Model):
    attachment = models.FileField(upload_to='file_attachments')
    label = models.CharField(max_length=100, blank=True, null=True, help_text='(optional)')
    position = models.ForeignKey('Position', related_name='file_attachments')

    def filename(self):
        return basename(self.attachment.name)
