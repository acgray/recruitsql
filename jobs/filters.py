import django_filters
from jobs.models import Position


class PositionFilterSet(django_filters.FilterSet):
    class Meta:
        model = Position
        # fields = ['organisation','period']
        fields = ['period', ]