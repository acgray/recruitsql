# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Removing unique constraint on 'Position', fields ['form']
        db.delete_unique(u'jobs_position', ['form_id'])

        # Adding field 'JobApplication.position'
        db.add_column(u'jobs_jobapplication', 'position',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['jobs.Position']),
                      keep_default=False)


        # Changing field 'Position.form'
        db.alter_column(u'jobs_position', 'form_id',
                        self.gf('django.db.models.fields.related.ForeignKey')(to=orm['jobs.ApplicationForm']))
        # Removing M2M table for field applicants on 'Position'
        db.delete_table(db.shorten_name(u'jobs_position_applicants'))


    def backwards(self, orm):
        # Deleting field 'JobApplication.position'
        db.delete_column(u'jobs_jobapplication', 'position_id')


        # Changing field 'Position.form'
        db.alter_column(u'jobs_position', 'form_id',
                        self.gf('django.db.models.fields.related.OneToOneField')(to=orm['jobs.ApplicationForm'],
                                                                                 unique=True))
        # Adding unique constraint on 'Position', fields ['form']
        db.create_unique(u'jobs_position', ['form_id'])

        # Adding M2M table for field applicants on 'Position'
        m2m_table_name = db.shorten_name(u'jobs_position_applicants')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('position', models.ForeignKey(orm[u'jobs.position'], null=False)),
            ('eldonuser', models.ForeignKey(orm[u'registration_withemail.eldonuser'], null=False))
        ))
        db.create_unique(m2m_table_name, ['position_id', 'eldonuser_id'])


    models = {
        u'jobs.applicationform': {
            'Meta': {'object_name': 'ApplicationForm'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro_text': ('django.db.models.fields.TextField', [], {})
        },
        u'jobs.field': {
            'Meta': {'ordering': "('order',)", 'object_name': 'Field'},
            'choices': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'default': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'blank': 'True'}),
            'field_type': ('django.db.models.fields.IntegerField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [],
                     {'related_name': "'fields'", 'to': u"orm['jobs.ApplicationForm']"}),
            'help_text': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'placeholder_text': (
            'django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'show_in_list': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'jobs.fieldentry': {
            'Meta': {'object_name': 'FieldEntry'},
            'entry': ('django.db.models.fields.related.ForeignKey', [],
                      {'related_name': "'fields'", 'to': u"orm['jobs.JobApplication']"}),
            'field_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'})
        },
        u'jobs.jobapplication': {
            'Meta': {'object_name': 'JobApplication'},
            'entry_time': ('django.db.models.fields.DateTimeField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.ApplicationForm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Position']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [],
                     {'to': u"orm['registration_withemail.EldonUser']", 'null': 'True'})
        },
        u'jobs.organisation': {
            'Meta': {'object_name': 'Organisation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"})
        },
        u'jobs.period': {
            'Meta': {'object_name': 'Period'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'jobs.position': {
            'Meta': {'object_name': 'Position'},
            'applicants': ('django.db.models.fields.related.ManyToManyField', [],
                           {'to': u"orm['registration_withemail.EldonUser']", 'symmetrical': 'False',
                            'through': u"orm['jobs.JobApplication']", 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.ApplicationForm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_description': ('django.db.models.fields.TextField', [], {}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Organisation']"}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Period']"}),
            'summary': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'registration_withemail.eldonuser': {
            'Meta': {'object_name': 'EldonUser'},
            'activation_key': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': (
            'django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['jobs']