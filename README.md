## RecruitSQL

This is a basic multi-tenant candidate/job application tracking system.  Originally developed for [New Turn](http://www.newturn.org.uk) and used by several other organisations.

### Main features

* Customisable per-position application forms
* Bulk customised email notifications to candidates
* Interview management: set up appointments and invite candidates to choose from available slots

### Contact

Adam Gray <info@adamgray.me.uk>

### License

THiS IS PROPRIETARY SOFTWARE.  THE SOURCE CODE IS PROVIDED FOR INFORMATION ONLY AND MAY DISAPPEAR AT ANY TIME.