# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Adding model 'EmailNotification'
        db.create_table(u'notifications_emailnotification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('job_application', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['jobs.JobApplication'])),
            ('template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['notifications.EmailTemplate'])),
            ('sent_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'notifications', ['EmailNotification'])


    def backwards(self, orm):
        # Deleting model 'EmailNotification'
        db.delete_table(u'notifications_emailnotification')


    models = {
        u'jobs.applicationform': {
            'Meta': {'object_name': 'ApplicationForm'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro_text': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'jobs.jobapplication': {
            'Meta': {'object_name': 'JobApplication'},
            'entry_time': ('django.db.models.fields.DateTimeField', [], {}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.ApplicationForm']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': (
            'django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Position']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'D'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [],
                     {'to': u"orm['registration_withemail.EldonUser']", 'null': 'True'})
        },
        u'jobs.organisation': {
            'Meta': {'object_name': 'Organisation'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"})
        },
        u'jobs.period': {
            'Meta': {'object_name': 'Period'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'jobs.position': {
            'Meta': {'object_name': 'Position'},
            'applicants': ('django.db.models.fields.related.ManyToManyField', [],
                           {'to': u"orm['registration_withemail.EldonUser']", 'symmetrical': 'False',
                            'through': u"orm['jobs.JobApplication']", 'blank': 'True'}),
            'form': ('django.db.models.fields.related.OneToOneField', [],
                     {'to': u"orm['jobs.ApplicationForm']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_description': ('django.db.models.fields.TextField', [], {}),
            'organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Organisation']"}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Period']"}),
            'summary': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'notifications.emailnotification': {
            'Meta': {'object_name': 'EmailNotification'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_application': (
            'django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.JobApplication']"}),
            'sent_date': ('django.db.models.fields.DateTimeField', [], {}),
            'template': (
            'django.db.models.fields.related.ForeignKey', [], {'to': u"orm['notifications.EmailTemplate']"})
        },
        u'notifications.emailtemplate': {
            'Meta': {'object_name': 'EmailTemplate'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'registration_withemail.eldonuser': {
            'Meta': {'object_name': 'EldonUser'},
            'activation_key': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': (
            'django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '75', 'blank': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['notifications']