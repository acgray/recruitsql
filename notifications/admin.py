from django.conf.urls import patterns, url
from django.contrib import admin
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.template import Template, Context, RequestContext
from django.utils.safestring import mark_safe
from jobs.models import JobApplication
from notifications.models import EmailTemplate


class EmailTemplateAdmin(admin.ModelAdmin):
    actions = ['preview']
    list_display = ['name', 'link_html']

    def preview(self, request, email_template_id):
        email_template = get_object_or_404(EmailTemplate, id=email_template_id)
        body = Template(email_template.body)
        subject = Template(email_template.subject)
        ja = JobApplication.objects.latest(field_name='entry_time')
        preview_context = Context({'object': ja})

        context = RequestContext(request)
        context['subject'] = subject.render(preview_context)
        context['body'] = body.render(preview_context)
        context['email_template'] = email_template
        return render_to_response('admin/notifications/emailtemplate_preview.html', context)

    def link_html(self, instance):
        return mark_safe(
            '<a href="%s">Preview</a>' % reverse('admin:notifications_emailtemplate_preview', args=(instance.id,)))

    link_html.short_description = "Actions"

    def json(self, request, id):
        qs = EmailTemplate.objects.filter(id=id)
        json = serializers.serialize('json', qs)
        return HttpResponse(json)

    def get_urls(self):
        urls = super(EmailTemplateAdmin, self).get_urls()

        extra_urls = patterns('',
                              url(r'(\d+)/preview', self.admin_site.admin_view(self.preview),
                                  name='notifications_emailtemplate_preview'),
                              url(r'(\d+)/json', self.admin_site.admin_view(self.json),
                                  name='notifications_emailtemplate_json'),
        )

        return extra_urls + urls


admin.site.register(EmailTemplate, EmailTemplateAdmin)