from django.template import Template, Context
from notifications.models import EmailNotification


def send_template_to_queryset(template, queryset):
    for obj in queryset:
        render_context = Context({'object': obj})
        rendered_data = {
            'subject': Template(template.subject).render(render_context),
            'body': Template(template.body).render(render_context),
        }

        EmailNotification.objects.create(
            job_application=obj,
            template=template,
            subject=rendered_data['subject'],
            body=rendered_data['body']
        )


