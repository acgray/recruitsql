from datetime import datetime
from django.core.mail import send_mail
from django.db import models

# Create your models here.

class EmailTemplate(models.Model):
    name = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    body = models.TextField(help_text="Use {{ object }} to access details of the current application")

    def __unicode__(self):
        return unicode(self.name)


class EmailNotification(models.Model):
    job_application = models.ForeignKey(to='jobs.JobApplication', related_name='email_notifications')
    template = models.ForeignKey(to='EmailTemplate', blank=True, null=True)
    sent_date = models.DateTimeField(auto_created=True)

    from_address = models.EmailField(blank=True, default='webmaster@newturn.org.uk')
    subject = models.CharField(max_length=255)
    body = models.TextField()

    def save(self, **kwargs):
        if not self.id:
            send_mail(self.subject, self.body, self.from_address or 'webmaster@newturn.org.uk',
                      [self.job_application.user.email])
            self.sent_date = datetime.now()

        return super(EmailNotification, self).save(**kwargs)