from django.conf.urls import patterns, url
from jobs_admin.views import BackendJobApplicationListView, BackendPositionListView

urlpatterns = patterns('',
    url('^$', BackendJobApplicationListView.as_view(), name='backend_index'),
    url('^positions$', BackendPositionListView.as_view(), name='backend_position_list'),
)