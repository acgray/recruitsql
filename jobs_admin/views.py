# Create your views here.
from django.views.generic import ListView
from jobs.models import JobApplication, Position


class BackendJobApplicationListView(ListView):
    model = JobApplication
    template_name = 'backend/jobapplication_list.html'

class BackendPositionListView(ListView):
    model = Position
    template_name = 'backend/position_list.html'

