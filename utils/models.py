from django.contrib.sites.models import Site
from django.db import models

# Create your models here.

class SiteConfig(models.Model):
    site = models.OneToOneField(Site)

    custom_css = models.CharField(max_length=50, default="", null=True, blank=True)
    custom_logo = models.CharField(max_length=50, default="", null=True, blank=True,
                                   help_text="Relative to images/ directory")
