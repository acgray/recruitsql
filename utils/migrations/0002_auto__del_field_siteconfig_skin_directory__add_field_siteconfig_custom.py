# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Deleting field 'SiteConfig.skin_directory'
        db.delete_column(u'utils_siteconfig', 'skin_directory')

        # Adding field 'SiteConfig.custom_css'
        db.add_column(u'utils_siteconfig', 'custom_css',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'SiteConfig.skin_directory'
        db.add_column(u'utils_siteconfig', 'skin_directory',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=10, blank=True),
                      keep_default=False)

        # Deleting field 'SiteConfig.custom_css'
        db.delete_column(u'utils_siteconfig', 'custom_css')


    models = {
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'utils.siteconfig': {
            'Meta': {'object_name': 'SiteConfig'},
            'custom_css': (
            'django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': (
            'django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sites.Site']", 'unique': 'True'})
        }
    }

    complete_apps = ['utils']