# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Adding field 'SiteConfig.custom_logo'
        db.add_column(u'utils_siteconfig', 'custom_logo',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SiteConfig.custom_logo'
        db.delete_column(u'utils_siteconfig', 'custom_logo')


    models = {
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'utils.siteconfig': {
            'Meta': {'object_name': 'SiteConfig'},
            'custom_css': (
            'django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True'}),
            'custom_logo': (
            'django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': (
            'django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sites.Site']", 'unique': 'True'})
        }
    }

    complete_apps = ['utils']