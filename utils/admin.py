from django.contrib import admin
from django.contrib.sites.models import Site
from utils.models import SiteConfig

admin.site.unregister(Site)


class SiteConfigInline(admin.StackedInline):
    model = SiteConfig


class ConfigurableSiteAdmin(admin.ModelAdmin):
    inlines = [SiteConfigInline, ]


admin.site.register(Site, ConfigurableSiteAdmin)