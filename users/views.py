# Create your views here.
import mimetypes
import os
from django import http
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import redirect, get_object_or_404
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.utils.http import http_date
from django.views.generic import UpdateView
from django.views.static import was_modified_since
from users.forms import UserProfileForm
from users.models import UserProfile


class UserProfileUpdateView(UpdateView):
    model = UserProfile
    form_class = UserProfileForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UserProfileUpdateView, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class):
        return form_class(RequestContext(self.request), **self.get_form_kwargs())

    def get(self, request, *args, **kwargs):
        self.request = request
        return super(UserProfileUpdateView, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            return self.model.objects.get(user=self.request.user)
        except self.model.DoesNotExist:
            return self.model(user=self.request.user)

    def get_success_url(self):
        return self.request.GET.get('next', reverse_lazy('jobapplication_list'))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        messages.add_message(self.request, messages.SUCCESS, "Your profile information has been updated.")
        return HttpResponseRedirect(self.get_success_url())


class UserUpdateView(UpdateView):
    model = get_user_model()
    template_name = 'users/user_form.html'

    def get_object(self, queryset=None):
        return self.request.user


def check_profile_view(request, *args, **kwargs):
    try:
        profile = UserProfile.objects.get(user=request.user)
    except UserProfile.DoesNotExist:
        messages.add_message(request, messages.INFO, 'Please fill in your profile before applying for any positions!')
        return redirect('update_profile')
    return HttpResponseRedirect(request.GET.get('next', '/'))


def get_cv_file(request, *args, **kwargs):
    profile = get_object_or_404(UserProfile, user_id=kwargs.get('uid'))

    if not request.user == profile.user and not request.user.is_superuser:
        return HttpResponseForbidden()

    fullpath = profile.cv.path
    statobj = os.stat(fullpath)
    mimetype, encoding, = mimetypes.guess_type(fullpath)

    if not was_modified_since(request.META.get('HTTP_IF_MODIFIED_SINCE'),
                              statobj.st_mtime, statobj.st_size):
        return http.HttpResponseNotModified(mimetype=mimetype)
    response = http.HttpResponse(open(fullpath, 'rb').read(), mimetype=mimetype)
    response["Last-Modified"] = http_date(statobj.st_mtime)
    response["Content-Length"] = statobj.st_size
    if encoding:
        response["Content-Encoding"] = encoding
    return response
