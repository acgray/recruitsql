import functools
from adminsortable.admin import SortableAdmin
from django.contrib import admin
from users.models import UserProfile, UserProfileField, UserProfileFieldEntry, UserProfileFieldGroup


class UserProfileFieldEntryInline(admin.TabularInline):
    model = UserProfileFieldEntry
    fields = ['field', 'value']
    readonly_fields = ['field']
    extra = 0
    can_delete = False

    def has_add_permission(self, request):
        return False


class UserProfileAdmin(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super(UserProfileAdmin, self).__init__(*args, **kwargs)

        def _get_field_value(field, obj):
            return obj.fields.get(field=field).value

        for f in UserProfileField.objects.all():
            setattr(self, str(f.slug), functools.partial(_get_field_value, f))
            getattr(self, str(f.slug)).short_description = f.label
            self.list_display.append(f.slug)

    inlines = [UserProfileFieldEntryInline, ]
    list_display = ['email', 'first_name', 'last_name']


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(UserProfileFieldGroup, SortableAdmin)
admin.site.register(UserProfileField, SortableAdmin)