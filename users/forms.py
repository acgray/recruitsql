from datetime import datetime
import os
from os.path import join
from uuid import uuid4
from django import forms
import django
from forms_builder.forms import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import ClearableFileInput, CheckboxInput
from django.template import Template
from django.utils.html import conditional_escape, format_html
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from forms_builder.forms.admin import fs
from forms_builder.forms.utils import split_choices
from forms_builder.forms import fields
from users.models import UserProfile, CV_ALLOWED_FORMATS, UserProfileField, UserProfileFieldEntry


class Email(forms.EmailField):
    def clean(self, value):
        super(Email, self).clean(value)
        try:
            User.objects.get(email=value)
            raise forms.ValidationError("This email is already registered.")
        except User.DoesNotExist:
            return value


class UserRegistrationForm(forms.Form):
    email = Email()
    password1 = forms.CharField(widget=forms.PasswordInput(), label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput(), label="Repeat your password")
    #email will be become username

    def clean_password2(self):
        if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            raise forms.ValidationError('Passwords are not the same')
        return self.data['password1']


class ProtectedFileWidget(ClearableFileInput):
    template_with_clear = '%(clear)s <label for="%(clear_checkbox_id)s" class="clear-checkbox-label">%(clear_checkbox_label)s</label>'
    # TODO: temporary hack - to be fixed in django 1.6
    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = '%(input)s'
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = format_html('<a class="tiny button" href="{0}">{1}</a>',
                                                   value.url,
                                                   'Click to view')
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False,
                                                                attrs={'id': checkbox_id, 'class': 'clear-checkbox'})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)


class UserProfileForm(forms.ModelForm):
    field_entry_model = UserProfileFieldEntry

    cv = forms.FileField(widget=ProtectedFileWidget(), required=False, label="Upload your CV",
                         help_text=UserProfile._meta.get_field('cv').help_text)
    date_of_birth = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = UserProfile
        exclude = ('user',)

    def __init__(self, context, *args, **kwargs):
        """
        Dynamically add each of the form fields for the given form model
        instance and its related field model instances.
        """
        self.form_fields = UserProfileField.objects.visible()
        initial = kwargs.pop("initial", {})
        # If a FormEntry instance is given to edit, stores it's field
        # values for using as initial data.
        field_entries = {}
        if kwargs.get("instance"):
            for field_entry in kwargs["instance"].fields.all():
                field_entries[field_entry.field_id] = field_entry.value
        super(UserProfileForm, self).__init__(*args, **kwargs)
        # Create the form fields.
        for field in self.form_fields:
            field_key = field.slug
            field_class = fields.CLASSES[field.field_type]
            field_widget = fields.WIDGETS.get(field.field_type)
            field_args = {"label": field.label, "required": field.required,
                          "help_text": field.help_text}
            arg_names = field_class.__init__.im_func.func_code.co_varnames
            if "max_length" in arg_names:
                field_args["max_length"] = settings.FIELD_MAX_LENGTH
            if "choices" in arg_names:
                field_args["choices"] = field.get_choices()
            if field_widget is not None:
                field_args["widget"] = field_widget
                #
            #   Initial value for field, in order of preference:
            #
            # - If a form model instance is given (eg we're editing a
            #   form response), then use the instance's value for the
            #   field.
            # - If the developer has provided an explicit "initial"
            #   dict, use it.
            # - The default value for the field instance as given in
            #   the admin.
            #
            initial_val = None
            try:
                initial_val = field_entries[field.id]
            except KeyError:
                try:
                    initial_val = initial[field_key]
                except KeyError:
                    initial_val = Template(field.default).render(context)
            if initial_val:
                if field.is_a(*fields.MULTIPLE):
                    initial_val = split_choices(initial_val)
                if field.field_type == fields.CHECKBOX:
                    initial_val = initial_val != "False"
                self.initial[field_key] = initial_val
            self.fields[field_key] = field_class(**field_args)

            if field.field_type == fields.DOB:
                now = datetime.now()
                years = range(now.year, now.year - 120, -1)
                self.fields[field_key].widget.years = years

            # Add identifying CSS classes to the field.
            css_class = field_class.__name__.lower()
            if field.required:
                css_class += " required"
                if (settings.USE_HTML5 and
                            field.field_type != fields.CHECKBOX_MULTIPLE):
                    self.fields[field_key].widget.attrs["required"] = ""
            self.fields[field_key].widget.attrs["class"] = css_class
            if field.placeholder_text and not field.default:
                text = field.placeholder_text
                self.fields[field_key].widget.attrs["placeholder"] = text

    def save(self, **kwargs):
        """
        Get/create a FormEntry instance and assign submitted values to
        related FieldEntry instances for each form field.
        """
        entry = super(UserProfileForm, self).save(commit=False)
        entry.entry_time = now()
        entry.save()
        entry_fields = entry.fields.values_list("field_id", flat=True)
        new_entry_fields = []
        for field in self.form_fields:
            field_key = field.slug
            value = self.cleaned_data[field_key]
            if value and self.fields[field_key].widget.needs_multipart_form:
                value = fs.save(join("forms", str(uuid4()), value.name), value)
            if isinstance(value, list):
                value = ", ".join([v.strip() for v in value])
            if field.id in entry_fields:
                field_entry = entry.fields.get(field_id=field.id)
                field_entry.value = value
                field_entry.save()
            else:
                new = {"profile": entry, "field_id": field.id, "value": value}
                new_entry_fields.append(self.field_entry_model(**new))
        if new_entry_fields:
            if django.VERSION >= (1, 4, 0):
                self.field_entry_model.objects.bulk_create(new_entry_fields)
            else:
                for field_entry in new_entry_fields:
                    field_entry.save()
        return entry

    def clean_cv(self):
        data = self.cleaned_data['cv']
        if data:
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext[1:].lower()
            if str(ext.lower()) not in CV_ALLOWED_FORMATS:
                raise ValidationError('File must be in one of the allowed formats: %s' % ' '.join(
                    [".%s" % f for f in CV_ALLOWED_FORMATS]))

        return data