import os
from adminsortable.models import Sortable
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse_lazy
from django.db import models

from django.db.models.fields.files import FieldFile
from django.shortcuts import resolve_url
from forms_builder.forms.models import AbstractField, FieldManager
from forms_builder.forms.utils import unique_slug, slugify

CV_ALLOWED_FORMATS = ['doc', 'docx', 'pdf', 'rtf']


def cv_upload_to(instance, filename):
    ext = os.path.splitext(filename)[1]
    return os.path.join(str(instance.user.id), "%s_CV%s" % (instance.user.id, ext.lower()))


storage = FileSystemStorage(location=settings.PROTECTED_MEDIA_ROOT)


class CVFieldFile(FieldFile):
    # def __str__(self):
    #     if self.name:
    #         return self.name
    #         return smart_text('Uploaded CV File')
    #     else:
    #         return smart_text('')
    #
    # def __unicode__(self):
    #     if self.name:
    #         return u'Uploaded CV File'
    #     else:
    #         return u''


    def _get_url(self):
        self._require_file()
        return self.instance.get_cv_url()

    url = property(_get_url)


class CVFileField(models.FileField):
    attr_class = CVFieldFile


from south.modelsinspector import add_introspection_rules

add_introspection_rules([], ["^users\.models\.CVFileField"])


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    telephone = models.CharField(max_length=255)
    date_of_birth = models.DateField(max_length=255)

    cv = CVFileField(storage=storage, upload_to=cv_upload_to,
                     help_text="Not required but may be necessary for some applications. Accepted formats: %s" % ' '.join(
                         ['.%s' % f for f in CV_ALLOWED_FORMATS]), blank=True, verbose_name='Upload your CV')

    @property
    def email(self):
        return self.user.email

    def get_absolute_url(self):
        return reverse_lazy('update_profile')

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def is_complete(self, require_cv=False):
        base_complete = self.first_name and self.last_name and self.telephone and self.date_of_birth
        cv_complete = self.cv or (not require_cv)   # require CV if it is required
        extra_complete = True
        for f in self.fields.all():
            if f.field.required and not f.value:
                extra_complete = False
                break

        return base_complete and extra_complete and cv_complete

    def get_cv_url(self):
        if self.cv.file:
            return resolve_url('get_cv', uid=self.user.id)
        else:
            return None


class UserProfileFieldEntry(models.Model):
    field = models.ForeignKey('UserProfileField', null=True)
    value = models.CharField(max_length=255,
                             null=True)
    profile = models.ForeignKey('UserProfile', related_name='fields')


class UserProfileField(AbstractField, Sortable):
    objects = FieldManager()

    class Meta(Sortable.Meta):
        pass

    def delete(self, using=None):
        UserProfileFieldEntry.objects.filter(field_id=self.id).delete()
        return super(UserProfileField, self).delete(using=using)

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = slugify(self).replace('-', '_')
            self.slug = unique_slug(UserProfileField.objects, "slug", slug)
        return super(AbstractField, self).save(*args, **kwargs)


class UserProfileFieldGroup(Sortable):
    name = models.CharField(max_length=255)

    class Meta(Sortable.Meta):
        pass

    def __unicode__(self):
        return unicode(self.name)

