from fabric.api import *

env.hosts = ['web325.webfaction.com']
env.user = 'adamgray'
env.virtualenv = 'recruitsql'
env.project_dir = '/home/%s/webapps/recruitsql_django/recruitsql' % env.user


def manage_py(command):
    with cd(env.project_dir):
        with prefix('workon recruitsql'):
            run('./manage.py %s' % command)


def collectstatic():
    manage_py('collectstatic --noinput')


def migrate():
    manage_py('migrate --all')


def git_pull():
    with cd(env.project_dir):
        run('git pull')


def install_requirements():
    with cd(env.project_dir):
        with prefix('workon recruitsql'):
            run('pip install -r requirements.txt')


def restart():
    with cd(env.project_dir):
        run('supervisorctl restart recruitsql')


def deploy():
    with cd('/home/%(user)s/webapps/recruitsql_django/recruitsql' % env):
        run('git pull')
        install_requirements()
        migrate()
        collectstatic()
        restart()

